<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar OOP</title>
</head>
<body>
<?php
require_once ("animal.php");
require_once ("frog.php");
require_once ("ape.php");


$sheep = new Animal("shaun");

echo "<h3>Release 0</h3>";
echo "Name : <b>". $sheep->name ."</b><br>"; //tampil :  "shaun"
echo $sheep->get_legs() ."<br>";//tampil : 4
echo "Cold Blooded : " . $sheep->cold_blooded ."<br>"; //tampil :  "no"
echo "<hr>";

echo "<h3>Release 1</h3>";
$ampibi = new Frog("buduk");
echo "Name : ". $ampibi->name ."</b><br>"; //tampil :  "buduk"
echo $ampibi->get_legs() ."<br>"; //tampil :  4
echo "Cold Blooded : " . $ampibi->cold_blooded ."<br>"; //tampil :  "no"
echo $ampibi->jump() ."<br>";//tampil : Hop Hop
echo "<br>";

$mamalia = new Ape("Kera Sakti");
echo "Name : ". $mamalia->name ."</b><br>"; //tampil :  "kera sakti"
echo $mamalia->get_legs() ."<br>"; //tampil :  2
echo "Cold Blooded : " . $mamalia->cold_blooded ."<br>"; //tampil :  "no"
echo $mamalia->yell() ."<br>";//tampil : Auouo

?>


</body>
</html>